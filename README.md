# Musical Typing

Musical Typing is an implementation for the Jack Audio Connection Kit of certain similar software that we prefer not to mention. It's meant to ease the switch towards JACK-based products, and to provide similar functionality to jack-keyboard.

Dependencies
---
Musical Typing requires Python 3 (python 2 may work but has not been tested), Qt5, and PySide2, which are not included here. When running in its main form, it also depends on jack-send-midi, which is included here. For licensing reasons, a disconnected, demo mode which does not depend on jack-send-midi is also available.

Usage
---
JACK mode: `$ python3 musicaltyping.py`
Non-JACK demo mode: `$ python3 musicaltyping.py --demo`

License
---
Musical Typing, except where noted otherwise, is dual-licensed under the Mozilla Public License, version 2.0 and the Creative Commons Share-Alike Attribution license, version 4.0 International. Parts of Musical Typing, under certain circumstances, may link with jack-send-midi, a program by taylor.fish. A copy of jack-send-midi and its source is also included with Musical Typing, but is not available under these licenses. jack-send-midi is available under the GNU General Public License, version 3 or later. If you run Musical Typing as such that it interact with jack-send-midi, the combined work that you are using is licensed under the GNU General Public License, version 3 or later. This is possible under both of the licenses of the rest of Musical Typing, both of which allow for compatibility with the GPL.
