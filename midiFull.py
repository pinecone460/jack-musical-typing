# This Python file uses the following encoding: utf-8

from jack_send_midi import Client

client = Client(name="Musical Typing")
def sendNoteEvent(note,status,velocity,channel):
    if status == 1:
        client.send_message(bytes([0b10010000 | channel, note, velocity]))
        #print([channel, note, velocity])
    elif status == 0:
        client.send_message(bytes([0b10000000 | channel, note, velocity]))

def sendSustainEvent(status,channel):
    client.send_message(bytes([(0b10110000 | channel), 0b01000000, 0b00000000 | status*127]))

def sendPanicEvent():
    for i in range(16):
        client.send_message(bytes([0b10110000 | i, 0b01111000, 0b00000000]))
        client.send_message(bytes([0b10110000 | i, 0b01111011, 0b00000000]))

def sendPitchBendUpdate(value):
    pass

def sendModulationUpdate(value):
    pass

def sendProgUpdate(value):
    pass

def sendBankUpdate(value):
    pass

