# This Python file uses the following encoding: utf-8
import sys
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import functools

if '--demo' in sys.argv:
    import midiDemo as midi
else:
    import midiFull as midi

# There are various comments here that try to denote what is not from the UI file. They are no longer accurate.

# Todo:
    # *connect other JACK MIDI features
    # Implement "Connect to:" menu
    # *implement some sort of accessibility
    # fix ZynAddSubFX bug

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(830, 320)
        MainWindow.setMinimumSize(QSize(830, 320))
        MainWindow.setMaximumSize(QSize(830, 320))
        font = QFont()
        font.setFamily(u"Sans 53")
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        MainWindow.setFont(font)
        MainWindow.setStyleSheet(u"background-color:#bbb;font:Open Sans Light;")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.btnA = QPushButton(self.centralwidget)
        self.btnA.setObjectName(u"btnA")
        self.btnA.setGeometry(QRect(140, 70, 51, 171))
        self.btnA.setStyleSheet(u"background-color:white;color:black;border:1px solid grey;border-radius:6;")
        self.btnS = QPushButton(self.centralwidget)
        self.btnS.setObjectName(u"btnS")
        self.btnS.setGeometry(QRect(200, 70, 51, 171))
        self.btnS.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnD = QPushButton(self.centralwidget)
        self.btnD.setObjectName(u"btnD")
        self.btnD.setGeometry(QRect(260, 70, 51, 171))
        self.btnD.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnG = QPushButton(self.centralwidget)
        self.btnG.setObjectName(u"btnG")
        self.btnG.setGeometry(QRect(380, 70, 51, 171))
        self.btnG.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnH = QPushButton(self.centralwidget)
        self.btnH.setObjectName(u"btnH")
        self.btnH.setGeometry(QRect(440, 70, 51, 171))
        self.btnH.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnJ = QPushButton(self.centralwidget)
        self.btnJ.setObjectName(u"btnJ")
        self.btnJ.setGeometry(QRect(500, 70, 51, 171))
        self.btnJ.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnK = QPushButton(self.centralwidget)
        self.btnK.setObjectName(u"btnK")
        self.btnK.setGeometry(QRect(560, 70, 51, 171))
        self.btnK.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnL = QPushButton(self.centralwidget)
        self.btnL.setObjectName(u"btnL")
        self.btnL.setGeometry(QRect(620, 70, 51, 171))
        self.btnL.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnSemicolon = QPushButton(self.centralwidget)
        self.btnSemicolon.setObjectName(u"btnSemicolon")
        self.btnSemicolon.setGeometry(QRect(680, 70, 51, 171))
        self.btnSemicolon.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnApostrophe = QPushButton(self.centralwidget)
        self.btnApostrophe.setObjectName(u"btnApostrophe")
        self.btnApostrophe.setGeometry(QRect(740, 70, 51, 171))
        self.btnApostrophe.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnW = QPushButton(self.centralwidget)
        self.btnW.setObjectName(u"btnW")
        self.btnW.setGeometry(QRect(170, 70, 51, 61))
        self.btnW.setStyleSheet(u"background-color:#333;color:white;border-radius:6;")
        self.btnF = QPushButton(self.centralwidget)
        self.btnF.setObjectName(u"btnF")
        self.btnF.setGeometry(QRect(320, 70, 51, 171))
        self.btnF.setStyleSheet(u"background-color:white;color:black;border-radius:6;border:1px solid grey;")
        self.btnE = QPushButton(self.centralwidget)
        self.btnE.setObjectName(u"btnE")
        self.btnE.setGeometry(QRect(230, 70, 51, 61))
        self.btnE.setStyleSheet(u"background-color:#333;color:white;border-radius:6;")
        self.btnT = QPushButton(self.centralwidget)
        self.btnT.setObjectName(u"btnT")
        self.btnT.setGeometry(QRect(350, 70, 51, 61))
        self.btnT.setStyleSheet(u"background-color:#333;color:white;border-radius:6;")
        self.btnY = QPushButton(self.centralwidget)
        self.btnY.setObjectName(u"btnY")
        self.btnY.setGeometry(QRect(410, 70, 51, 61))
        self.btnY.setStyleSheet(u"background-color:#333;color:white;border-radius:6;")
        self.btnU = QPushButton(self.centralwidget)
        self.btnU.setObjectName(u"btnU")
        self.btnU.setGeometry(QRect(470, 70, 51, 61))
        self.btnU.setStyleSheet(u"background-color:#333;color:white;border-radius:6;")
        self.btnO = QPushButton(self.centralwidget)
        self.btnO.setObjectName(u"btnO")
        self.btnO.setGeometry(QRect(590, 70, 51, 61))
        self.btnO.setStyleSheet(u"background-color:#333;color:white;border-radius:6;")
        self.btnP = QPushButton(self.centralwidget)
        self.btnP.setObjectName(u"btnP")
        self.btnP.setGeometry(QRect(650, 70, 51, 61))
        self.btnP.setStyleSheet(u"background-color:#333;color:white;border-radius:6;")
        self.pitchBendLabel = QLabel(self.centralwidget)
        self.pitchBendLabel.setObjectName(u"pitchBendLabel")
        self.pitchBendLabel.setGeometry(QRect(10, 10, 81, 18))
        self.pitchBendLabel.setStyleSheet(u"color:grey;")
        self.pitchBendValue = QLabel(self.centralwidget)
        self.pitchBendValue.setObjectName(u"pitchBendValue")
        self.pitchBendValue.setGeometry(QRect(85, 10, 58, 18))
        self.pitchBendValue.setStyleSheet(u"color:black;")
        self.octaveValue = QLabel(self.centralwidget)
        self.octaveValue.setObjectName(u"octaveValue")
        self.octaveValue.setGeometry(QRect(110, 250, 58, 18))
        self.octaveValue.setStyleSheet(u"color:black;")
        self.octaveLabel = QLabel(self.centralwidget)
        self.octaveLabel.setObjectName(u"octaveLabel")
        self.octaveLabel.setGeometry(QRect(50, 250, 51, 18))
        self.octaveLabel.setStyleSheet(u"color:grey;")
        self.modulationLabel = QLabel(self.centralwidget)
        self.modulationLabel.setObjectName(u"modulationLabel")
        self.modulationLabel.setGeometry(QRect(590, 10, 81, 18))
        self.modulationLabel.setStyleSheet(u"color:grey;")
        self.btnZ = QPushButton(self.centralwidget)
        self.btnZ.setObjectName(u"btnZ")
        self.btnZ.setGeometry(QRect(170, 250, 51, 51))
        self.btnZ.setStyleSheet(u"background-color:#FE4;color:black;border-radius:6;border:1px solid grey;")
        self.btnX = QPushButton(self.centralwidget)
        self.btnX.setObjectName(u"btnX")
        self.btnX.setGeometry(QRect(230, 250, 51, 51))
        self.btnX.setStyleSheet(u"background-color:#FE4;color:black;border-radius:6;border:1px solid grey;")
        self.btnC = QPushButton(self.centralwidget)
        self.btnC.setObjectName(u"btnC")
        self.btnC.setGeometry(QRect(290, 250, 51, 51))
        self.btnC.setStyleSheet(u"background-color:#FA3;color:black;border-radius:6;border:1px solid grey;")
        self.btnV = QPushButton(self.centralwidget)
        self.btnV.setObjectName(u"btnV")
        self.btnV.setGeometry(QRect(350, 250, 51, 51))
        self.btnV.setStyleSheet(u"background-color:#FA3;color:black;border-radius:6;border:1px solid grey;")
        self.velocityValue = QLabel(self.centralwidget)
        self.velocityValue.setObjectName(u"velocityValue")
        self.velocityValue.setGeometry(QRect(480, 250, 58, 18))
        self.velocityValue.setStyleSheet(u"color:black;")
        self.velocityLabel = QLabel(self.centralwidget)
        self.velocityLabel.setObjectName(u"velocityLabel")
        self.velocityLabel.setGeometry(QRect(420, 250, 51, 18))
        self.velocityLabel.setStyleSheet(u"color:grey;")
        self.btnTab = QPushButton(self.centralwidget)
        self.btnTab.setObjectName(u"btnTab")
        self.btnTab.setGeometry(QRect(20, 70, 101, 51))
        self.btnTab.setStyleSheet(u"background-color:#66FF6F;color:black;border-radius:6;border:1px solid grey;")
        self.btn1 = QPushButton(self.centralwidget)
        self.btn1.setObjectName(u"btn1")
        self.btn1.setGeometry(QRect(110, 10, 51, 51))
        self.btn1.setStyleSheet(u"background-color:#3CF;color:black;border-radius:6;border:1px solid grey;")
        self.btn2 = QPushButton(self.centralwidget)
        self.btn2.setObjectName(u"btn2")
        self.btn2.setGeometry(QRect(170, 10, 51, 51))
        self.btn2.setStyleSheet(u"background-color:#3CF;color:black;border-radius:6;border:1px solid grey;")
        self.btn3 = QPushButton(self.centralwidget)
        self.btn3.setObjectName(u"btn3")
        self.btn3.setGeometry(QRect(230, 10, 51, 51))
        self.btn3.setStyleSheet(u"background-color:#53A;color:white;border-radius:6;border:1px solid grey;")
        self.btn4 = QPushButton(self.centralwidget)
        self.btn4.setObjectName(u"btn4")
        self.btn4.setGeometry(QRect(290, 10, 51, 51))
        self.btn4.setStyleSheet(u"background-color:#A6F;color:black;border-radius:6;border:1px solid grey;")
        self.btn5 = QPushButton(self.centralwidget)
        self.btn5.setObjectName(u"btn5")
        self.btn5.setGeometry(QRect(350, 10, 51, 51))
        self.btn5.setStyleSheet(u"background-color:#A6F;color:black;border-radius:6;border:1px solid grey;")
        self.btn6 = QPushButton(self.centralwidget)
        self.btn6.setObjectName(u"btn6")
        self.btn6.setGeometry(QRect(410, 10, 51, 51))
        self.btn6.setStyleSheet(u"background-color:#A6F;color:black;border-radius:6;border:1px solid grey;")
        self.btn7 = QPushButton(self.centralwidget)
        self.btn7.setObjectName(u"btn7")
        self.btn7.setGeometry(QRect(470, 10, 51, 51))
        self.btn7.setStyleSheet(u"background-color:#A6F;color:black;border-radius:6;border:1px solid grey;")
        self.btn8 = QPushButton(self.centralwidget)
        self.btn8.setObjectName(u"btn8")
        self.btn8.setGeometry(QRect(530, 10, 51, 51))
        self.btn8.setStyleSheet(u"background-color:#A6F;color:black;border-radius:6;border:1px solid grey;")
        self.btnDelete = QPushButton(self.centralwidget)
        self.btnDelete.setObjectName(u"btnDelete")
        self.btnDelete.setGeometry(QRect(690, 10, 101, 51))
        self.btnDelete.setStyleSheet(u"background-color:#B00;color:white;border-radius:6;border:1px solid grey;")
        self.connectToBox = QComboBox(self.centralwidget)
        self.connectToBox.addItem("")
        self.connectToBox.setObjectName(u"connectToBox")
        self.connectToBox.setGeometry(QRect(710, 280, 141, 28))
        self.connectToBox.setAutoFillBackground(False)
        self.connectToBox.setStyleSheet(u"border:1px solid grey; border-radius:3;color:black;background-color:white;")
        self.connectToBox.setFrame(True)
        self.connectToLabel = QLabel(self.centralwidget)
        self.connectToLabel.setObjectName(u"connectToLabel")
        self.connectToLabel.setGeometry(QRect(710, 260, 81, 20))
        self.connectToLabel.setStyleSheet(u"color:grey;")
        self.channelField = QLineEdit(self.centralwidget)
        self.channelField.setObjectName(u"channelField")
        self.channelField.setGeometry(QRect(560, 280, 41, 28))
        self.channelField.setStyleSheet(u"background-color:white;color:black;border-radius:3;border:1px solid grey;")
        self.channelLabel = QLabel(self.centralwidget)
        self.channelLabel.setObjectName(u"channelLabel")
        self.channelLabel.setGeometry(QRect(560, 260, 51, 18))
        self.channelLabel.setStyleSheet(u"color:grey;")
        self.bankLabel = QLabel(self.centralwidget)
        self.bankLabel.setObjectName(u"bankLabel")
        self.bankLabel.setGeometry(QRect(610, 260, 51, 18))
        self.bankLabel.setStyleSheet(u"color:grey;")
        self.bankField = QLineEdit(self.centralwidget)
        self.bankField.setObjectName(u"bankField")
        self.bankField.setGeometry(QRect(610, 280, 41, 28))
        self.bankField.setStyleSheet(u"background-color:white;color:black;border-radius:3;border:1px solid grey;")
        self.progField = QLineEdit(self.centralwidget)
        self.progField.setObjectName(u"progField")
        self.progField.setGeometry(QRect(660, 280, 41, 28))
        self.progField.setStyleSheet(u"background-color:white;color:black;border-radius:3;border:1px solid grey;")
        self.progLabel = QLabel(self.centralwidget)
        self.progLabel.setObjectName(u"progLabel")
        self.progLabel.setGeometry(QRect(660, 260, 51, 18))
        self.progLabel.setStyleSheet(u"color:grey;")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        # start not from the UI file




        self.bankField.setMaxLength(3)
        self.progField.setMaxLength(3)
        self.channelField.setMaxLength(3)

        self.preparePressedStylesheets()

        self.setValues()

        self.bindGUI()

        self.progUpdate()
        self.bankUpdate()

        self.disableFocus()

        # end not from the UI file

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Musical Typing", None))
        self.btnA.setText(QCoreApplication.translate("MainWindow", u"A", None))
        self.btnS.setText(QCoreApplication.translate("MainWindow", u"S", None))
        self.btnD.setText(QCoreApplication.translate("MainWindow", u"D", None))
        self.btnG.setText(QCoreApplication.translate("MainWindow", u"G", None))
        self.btnH.setText(QCoreApplication.translate("MainWindow", u"H", None))
        self.btnJ.setText(QCoreApplication.translate("MainWindow", u"J", None))
        self.btnK.setText(QCoreApplication.translate("MainWindow", u"K", None))
        self.btnL.setText(QCoreApplication.translate("MainWindow", u"L", None))
        self.btnSemicolon.setText(QCoreApplication.translate("MainWindow", u";", None))
        self.btnApostrophe.setText(QCoreApplication.translate("MainWindow", u"'", None))
        self.btnW.setText(QCoreApplication.translate("MainWindow", u"W", None))
        self.btnF.setText(QCoreApplication.translate("MainWindow", u"F", None))
        self.btnE.setText(QCoreApplication.translate("MainWindow", u"E", None))
        self.btnT.setText(QCoreApplication.translate("MainWindow", u"T", None))
        self.btnY.setText(QCoreApplication.translate("MainWindow", u"Y", None))
        self.btnU.setText(QCoreApplication.translate("MainWindow", u"U", None))
        self.btnO.setText(QCoreApplication.translate("MainWindow", u"O", None))
        self.btnP.setText(QCoreApplication.translate("MainWindow", u"P", None))
        self.pitchBendLabel.setText(QCoreApplication.translate("MainWindow", u"Pitch Bend:", None))
        self.pitchBendValue.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.octaveValue.setText(QCoreApplication.translate("MainWindow", u"C2", None))
        self.octaveLabel.setText(QCoreApplication.translate("MainWindow", u"Octave:", None))
        self.modulationLabel.setText(QCoreApplication.translate("MainWindow", u"Modulation", None))
        self.btnZ.setText(QCoreApplication.translate("MainWindow", u"Z (-)", None))
        self.btnX.setText(QCoreApplication.translate("MainWindow", u"X (+)", None))
        self.btnC.setText(QCoreApplication.translate("MainWindow", u"C (-)", None))
        self.btnV.setText(QCoreApplication.translate("MainWindow", u"V (+)", None))
        self.velocityValue.setText(QCoreApplication.translate("MainWindow", u"100", None))
        self.velocityLabel.setText(QCoreApplication.translate("MainWindow", u"Velocity:", None))
        self.btnTab.setText(QCoreApplication.translate("MainWindow", u"tab (sustain)", None))
        self.btn1.setText(QCoreApplication.translate("MainWindow", u"1 (-)", None))
        self.btn2.setText(QCoreApplication.translate("MainWindow", u"2 (+)", None))
        self.btn3.setText(QCoreApplication.translate("MainWindow", u"3 (off)", None))
        self.btn4.setText(QCoreApplication.translate("MainWindow", u"4", None))
        self.btn5.setText(QCoreApplication.translate("MainWindow", u"5", None))
        self.btn6.setText(QCoreApplication.translate("MainWindow", u"6", None))
        self.btn7.setText(QCoreApplication.translate("MainWindow", u"7", None))
        self.btn8.setText(QCoreApplication.translate("MainWindow", u"8", None))
        self.btnDelete.setText(QCoreApplication.translate("MainWindow", u"delete (panic)", None))
        self.connectToBox.setItemText(0, QCoreApplication.translate("MainWindow", u"None", None))

        self.connectToLabel.setText(QCoreApplication.translate("MainWindow", u"Connect to:", None))
        self.channelField.setText(QCoreApplication.translate("MainWindow", u"1", None))
        self.channelLabel.setText(QCoreApplication.translate("MainWindow", u"Ch.", None))
        self.bankLabel.setText(QCoreApplication.translate("MainWindow", u"Bnk.", None))
        self.bankField.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.progField.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.progLabel.setText(QCoreApplication.translate("MainWindow", u"Prog.", None))
    # retranslateUi

    # start not from the UI file

    def setValues(self):
        self.currentNotes = []
        self.velocity = 60
        self.octave = 2
        self.modulationKeynumber = 0
        self.modulationKeyValues = [0,32,54,76,98,127]
        self.pitchBend = 0
        self.midichannel = 1
        self.midiprog = 0
        self.midibank = 0
        # check on these last three to see what they should be
        self.velocityCounter = 5
        self.pitchBendCounter = 20
        self.modulationMultiplier = 1
        self.modulationKeys = [self.btn3, self.btn4, self.btn5, self.btn6, self.btn7, self.btn8]
        self.buttons = [self.btn3,self.btn4,self.btn5,self.btn6,self.btn7,self.btn8,self.btnA,self.btnS,self.btnD,self.btnF,self.btnG,self.btnH,self.btnJ,self.btnK,self.btnL,self.btnSemicolon,self.btnApostrophe,self.btnW,self.btnE,self.btnT,self.btnY,self.btnU,self.btnO,self.btnP,self.btnZ,self.btnX,self.btnC,self.btnV,self.btnTab,self.btnDelete,self.btn1,self.btn2]
        for i in range(len(self.buttons)):
            self.buttons[i].pressedState = 0
        self.noteBindings = {
        self.btnA:0,
        self.btnS:2,
        self.btnD:4,
        self.btnF:5,
        self.btnG:7,
        self.btnH:9,
        self.btnJ:11,
        self.btnK:12,
        self.btnL:14,
        self.btnSemicolon:16,
        self.btnApostrophe:17,
        self.btnW:1,
        self.btnE:3,
        self.btnT:6,
        self.btnY:8,
        self.btnU:10,
        self.btnO:13,
        self.btnP:15
        }
        self.keybindings = {
        Qt.Key_A:self.btnA,
        Qt.Key_S:self.btnS,
        Qt.Key_D:self.btnD,
        Qt.Key_F:self.btnF,
        Qt.Key_G:self.btnG,
        Qt.Key_H:self.btnH,
        Qt.Key_J:self.btnJ,
        Qt.Key_K:self.btnK,
        Qt.Key_L:self.btnL,
        Qt.Key_Semicolon:self.btnSemicolon,
        Qt.Key_Apostrophe:self.btnApostrophe,
        Qt.Key_W:self.btnW,
        Qt.Key_E:self.btnE,
        Qt.Key_T:self.btnT,
        Qt.Key_Y:self.btnY,
        Qt.Key_U:self.btnU,
        Qt.Key_O:self.btnO,
        Qt.Key_P:self.btnP,
        Qt.Key_1:self.btn1,
        Qt.Key_2:self.btn2,
        Qt.Key_3:self.btn3,
        Qt.Key_4:self.btn4,
        Qt.Key_5:self.btn5,
        Qt.Key_6:self.btn6,
        Qt.Key_7:self.btn7,
        Qt.Key_8:self.btn8,
        Qt.Key_Z:self.btnZ,
        Qt.Key_X:self.btnX,
        Qt.Key_C:self.btnC,
        Qt.Key_V:self.btnV,
        Qt.Key_Delete:self.btnDelete,
        Qt.Key_Backspace:self.btnDelete,
        Qt.Key_Tab:self.btnTab,
        }
        self.connectToOptions = ["None"]
        self.relabel()

    def relabel(self):
        self.velocityValue.setText(str(self.velocity))
        self.octaveValue.setText("C" + str(self.octave))
        self.pitchBendValue.setText(str(self.pitchBend))
        for i in range(len(self.modulationKeys)):
            if self.modulationKeynumber == i:
                self.modulationKeys[i].pps = self.ppsDarkPurple
            else:
                self.modulationKeys[i].pps = self.ppsPurple
            self.modulationKeys[i].setStyleSheet(self.modulationKeys[i].pps[self.modulationKeys[i].pressedState])
        if not (self.channelField.text() == '' and self.channelField.hasFocus()):
            self.channelField.setText(str(self.midichannel))
        if not (self.progField.text() == '' and self.progField.hasFocus()):
            self.progField.setText(str(self.midiprog))
        if not (self.bankField.text() == '' and self.bankField.hasFocus()):
            self.bankField.setText(str(self.midibank))


    def preparePressedStylesheets(self):
        self.ppsBlack = [u"background-color:#333;color:white;border-radius:6;",u"background-color:#222;color:white;border-radius:6;"]
        self.ppsGreen = [u"background-color:#66FF6F;color:black;border-radius:6;border:1px solid grey;",u"background-color:#44DD4D;color:black;border-radius:6;border:1px solid grey;"]
        self.ppsRed = [u"background-color:#B00;color:white;border-radius:6;border:1px solid grey;",u"background-color:#900;color:white;border-radius:6;border:1px solid grey;"]
        self.ppsWhite = [u"background-color:white;color:black;border:1px solid grey;border-radius:6;",u"background-color:#DDD;color:black;border:1px solid grey;border-radius:6;"]
        self.ppsOrange = [u"background-color:#FA3;color:black;border-radius:6;border:1px solid grey;",u"background-color:#D81;color:black;border-radius:6;border:1px solid grey;"]
        self.ppsYellow = [u"background-color:#FE4;color:black;border-radius:6;border:1px solid grey;",u"background-color:#DC2;color:black;border-radius:6;border:1px solid grey;"]
        self.ppsPurple = [u"background-color:#A6F;color:black;border-radius:6;border:1px solid grey;",u"background-color:#84D;color:black;border-radius:6;border:1px solid grey;"]
        self.ppsDarkPurple = [u"background-color:#53A;color:white;border-radius:6;border:1px solid grey;",u"background-color:#318;color:white;border-radius:6;border:1px solid grey;"]
        self.ppsBlue = [u"background-color:#3CF;color:black;border-radius:6;border:1px solid grey;",u"background-color:#1AD;color:black;border-radius:6;border:1px solid grey;"]
        for i in [self.btnW,self.btnE,self.btnT,self.btnY,self.btnU,self.btnO,self.btnP]:
            i.pps = self.ppsBlack
        for i in [self.btnTab]:
            i.pps = self.ppsGreen
        for i in [self.btnDelete]:
            i.pps = self.ppsRed
        for i in [self.btnA,self.btnS,self.btnD,self.btnF,self.btnG,self.btnH,self.btnJ,self.btnK,self.btnL,self.btnSemicolon,self.btnApostrophe]:
            i.pps = self.ppsWhite
        for i in [self.btnC,self.btnV]:
            i.pps = self.ppsOrange
        for i in [self.btnZ,self.btnX]:
            i.pps = self.ppsYellow
        for i in [self.btn4,self.btn5,self.btn6,self.btn7,self.btn8]:
            i.pps = self.ppsPurple
        for i in [self.btn3]:
            i.pps = self.ppsDarkPurple
        for i in [self.btn1,self.btn2]:
            i.pps = self.ppsBlue

    def bindGUI(self):
        for i in self.buttons:
            i.pressed.connect(functools.partial(self.buttonPressDown,i))
            i.released.connect(functools.partial(self.buttonPressUp,i))
        self.connectToBox.activated.connect(self.connectToItemSelected)
        self.channelField.textChanged.connect(self.channelUpdate)
        self.progField.textChanged.connect(self.progUpdate)
        self.bankField.textChanged.connect(self.bankUpdate)
        self.channelField.editingFinished.connect(self.relabel)
        self.progField.editingFinished.connect(self.relabel)
        self.bankField.editingFinished.connect(self.relabel)

    def disableFocus(self):
        for i in self.buttons:
            i.setFocusPolicy(Qt.NoFocus)
        self.channelField.setFocusPolicy(Qt.ClickFocus)
        self.progField.setFocusPolicy(Qt.ClickFocus)
        self.bankField.setFocusPolicy(Qt.ClickFocus)
        self.connectToBox.setFocusPolicy(Qt.ClickFocus)
        self.centralwidget.setFocus()

    def connectToItemSelected(self,n):
        self.centralwidget.setFocus() # fix focus
        self.lastSelectedConnectToIndex = n
        self.connectToOption = self.connectToOptions[self.lastSelectedConnectToIndex]

    def buttonPressDown(self,button):
        button.pressedState = 1
        if button in self.noteBindings.keys():
            midi.sendNoteEvent(self.noteBindings[button]+self.octave*12,1,self.velocity,self.midichannel)
            self.currentNotes.append([self.noteBindings[button]+self.octave*12,1,self.velocity,self.midichannel])
        elif button == self.btnTab:
            midi.sendSustainEvent(1,self.midichannel)
        elif button == self.btnDelete:
            print(button)
            midi.sendPanicEvent()
        elif button == self.btnC:
            self.velocity -= self.velocityCounter
            if self.velocity < 0:
                self.velocity = 0
            self.relabel()
        elif button == self.btnV:
            self.velocity += self.velocityCounter
            if self.velocity > 127:
                self.velocity = 127
            self.relabel()
        elif button == self.btnZ:
            self.octave -= 1
            if self.octave < -2:
                self.octave = -2
            self.relabel()
        elif button == self.btnX:
            self.octave += 1
            if self.octave > 7:
                self.octave = 7
            self.relabel()
        elif button == self.btn1:
            self.pitchBend -= self.pitchBendCounter
            midi.sendPitchBendUpdate(self.pitchBend)
            self.relabel()
        elif button == self.btn2:
            self.pitchBend += self.pitchBendCounter
            midi.sendPitchBendUpdate(self.pitchBend)
            self.relabel()
        for i in range(len(self.modulationKeys)):
            if button == self.modulationKeys[i]:
                self.modulationKeynumber = i
                midi.sendModulationUpdate(self.modulationKeynumber)
                self.relabel()
        button.setStyleSheet(button.pps[1])

    def buttonPressUp(self,button):
        button.pressedState = 0
        if button in self.noteBindings.keys():
#            midi.sendNoteEvent(self.noteBindings[button]+self.octave*12,0,self.velocity,self.midichannel)
            for i in self.currentNotes:
                if (i[0] - (self.noteBindings[button]+self.octave*12)) % 12 == 0:
                    midi.sendNoteEvent(i[0],0,i[2],i[3])
                    self.currentNotes.remove(i)
        elif button == self.btnTab:
            midi.sendSustainEvent(0,self.midichannel)
        elif button == self.btn1:
            self.pitchBend += self.pitchBendCounter
            midi.sendPitchBendUpdate(self.pitchBend)
            self.relabel()
        elif button == self.btn2:
            self.pitchBend -= self.pitchBendCounter
            midi.sendPitchBendUpdate(self.pitchBend)
            self.relabel()
        button.setStyleSheet(button.pps[0])

    def progUpdate(self):
        if self.progField.text() != '' and not False in [i.isdigit() for i in self.progField.text()]:
            self.midiprog = int(self.progField.text())
            if self.midiprog > 127:
                self.midiprog = 127
            if self.midiprog < 0:
                self.midiprog = 0
            midi.sendProgUpdate(self.midiprog)
        self.relabel()

    def bankUpdate(self):
        if self.bankField.text() != '' and not False in [i.isdigit() for i in self.bankField.text()]:
            self.midibank = int(self.bankField.text())
            if self.midibank > 127:
                self.midibank = 127
            if self.midibank < 0:
                self.midibank = 0
                midi.sendProgUpdate(self.midiprog)
        self.relabel()

    def channelUpdate(self):
        self.buttonPressUp(self.btnTab)
        if self.channelField.text() != '' and not False in [i.isdigit() for i in self.channelField.text()]:
            self.midichannel = int(self.channelField.text())
            if self.midichannel > 15:
                self.midichannel = 15
            if self.midichannel < 0:
                self.midichannel = 0
        self.relabel()

    def upAll(self):
        for i in self.buttons:
            self.buttonPressUp(i)
        midi.sendPitchBendUpdate(0)
        self.pitchBend = 0





    # end not from the UI file

# start not from the UI file

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.setFocusPolicy(Qt.ClickFocus)
        self.ui.setupUi(self)
#        for i in self.ui.findChildren(QObject, ''):
#            print(i) # i.setFocusPolicy(Qt.NoFocus)


    def keyPressEvent(self,event):
        if event.key() in self.ui.keybindings.keys() and [self.ui.channelField.hasFocus(), self.ui.progField.hasFocus(), self.ui.bankField.hasFocus()] == [False, False, False] and True in [not event.isAutoRepeat(), event.key() in [Qt.Key_C, Qt.Key_V]]:
            self.ui.buttonPressDown(self.ui.keybindings[event.key()])

    def keyReleaseEvent(self,event):
        if event.key() in self.ui.keybindings.keys() and [self.ui.channelField.hasFocus(), self.ui.progField.hasFocus(), self.ui.bankField.hasFocus()] == [False, False, False] and True in [not event.isAutoRepeat(), event.key() in [Qt.Key_C, Qt.Key_V]]:
            self.ui.buttonPressUp(self.ui.keybindings[event.key()])


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.setFocusPolicy(Qt.ClickFocus)
    app.focusChanged.connect(window.ui.upAll) # this is wayyyy out of place but it needs to be here; it unpresses all buttons when you select or deselect a field
    window.show()

    sys.exit(app.exec_())

# end not from the UI file
